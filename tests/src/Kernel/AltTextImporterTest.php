<?php

namespace Drupal\Tests\alt_text_import_csv\Kernel;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\Tests\field\Traits\EntityReferenceFieldCreationTrait;
use Drupal\Tests\image\Kernel\ImageFieldCreationTrait;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests the alt text importer.
 *
 * @group alt_text_import_csv
 */
class AltTextImporterTest extends KernelTestBase {

  use EntityReferenceFieldCreationTrait;
  use ImageFieldCreationTrait;
  use MediaTypeCreationTrait;
  use NodeCreationTrait;
  use UserCreationTrait;
  use BlockCreationTrait;

  /**
   * The modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'user',
    'field',
    'node',
    'file',
    'image',
    'media',
    'block',
    'block_content',
    'path',
    'path_alias',
    'entity_usage',
    'alt_text_import_csv',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The alias repository service.
   *
   * @var \Drupal\path_alias\AliasRepositoryInterface
   */
  protected $aliasRepository;

  /**
   * The alt text importer service.
   *
   * @var \Drupal\alt_text_import_csv\AltTextImporter
   */
  protected $altTextImporter;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installSchema('file', ['file_usage']);
    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('node');
    $this->installEntitySchema('media');
    $this->installEntitySchema('block_content');
    $this->installEntitySchema('path_alias');
    $this->installSchema('entity_usage', 'entity_usage');

    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->fileSystem = $this->container->get('file_system');
    $this->aliasRepository = $this->container->get('path_alias.repository');
    $this->altTextImporter = $this->container->get('alt_text_import_csv.alt_text_importer');

    $this->setUpCurrentUser();
  }

  /**
   * Tests the alt text importer service with a media reference field.
   */
  public function testMediaField() {
    // Create the node type, media type, and reference field.
    $node_type_storage = $this->entityTypeManager->getStorage('node_type');
    $type = $node_type_storage->create([
      'type' => 'page',
      'name' => 'Page',
    ]);
    $type->save();

    $this->createMediaType('image', ['id' => 'test_image']);

    $this->createEntityReferenceField(
      'node',
      'page',
      'field_image',
      'Image',
      'media',
    );

    // Create the file, media, and node entities.
    $this->fileSystem->copy($this->root . '/core/misc/druplicon.png', 'public://druplicon.png');
    $image_file = $this->entityTypeManager->getStorage('file')->create([
      'uri' => 'public://druplicon.png',
    ]);
    $image_file->save();

    $media = $this->entityTypeManager->getStorage('media')->create([
      'bundle' => 'test_image',
      'field_media_image' => $image_file,
    ]);
    $media->save();

    $node = $this->createNode([
      'type' => 'page',
      'field_image' => $media,
      'path' => ['alias' => '/foo'],
    ]);
    $node->save();

    $stored_alias = $this->aliasRepository->lookupBySystemPath('/' . $node->toUrl()->getInternalPath(), $node->language()->getId());
    $this->assertEquals('/foo', $stored_alias['alias']);

    // Get the image URL. Note that this looks totally broken because of a core
    // bug, but will work in AltTextImporter because of how other paths are
    // returned by the test system.
    // See https://www.drupal.org/project/drupal/issues/3401247.
    // This gets us a broken URL of the form
    // /vfs://root/sites/simpletest/38522002/files/druplicon.png.
    $image_url = $image_file->createFileUrl();
    // Add a token, as image URLs copied by users will have these.
    $image_url .= '?itok=5IYrswxV';

    $this->altTextImporter->importAltText($image_url, 'http://example.com/foo', 'new alt text');

    $media = $this->reloadEntity($media);
    $this->assertEquals('new alt text', $media->field_media_image[0]->alt);
  }

  /**
   * Tests with a media reference field on the front page.
   */
  public function testFrontPageUrlMediaField() {
    // Create the node type, media type, and reference field.
    $node_type_storage = $this->entityTypeManager->getStorage('node_type');
    $type = $node_type_storage->create([
      'type' => 'page',
      'name' => 'Page',
    ]);
    $type->save();

    $this->createMediaType('image', ['id' => 'test_image']);

    $this->createEntityReferenceField(
      'node',
      'page',
      'field_image',
      'Image',
      'media',
    );

    // Create the file, media, and node entities.
    $this->fileSystem->copy($this->root . '/core/misc/druplicon.png', 'public://druplicon.png');
    $image_file = $this->entityTypeManager->getStorage('file')->create([
      'uri' => 'public://druplicon.png',
    ]);
    $image_file->save();

    $media = $this->entityTypeManager->getStorage('media')->create([
      'bundle' => 'test_image',
      'field_media_image' => $image_file,
    ]);
    $media->save();

    $node = $this->createNode([
      'type' => 'page',
      'field_image' => $media,
    ]);
    $node->save();

    // Configure 'node' as front page.
    $this->config('system.site')->set('page.front', '/node/' . $node->id())->save();

    $image_url = $image_file->createFileUrl();
    // Add a token, as image URLs copied by users will have these.
    $image_url .= '?itok=5IYrswxV';

    $this->altTextImporter->importAltText($image_url, 'http://example.com/', 'new alt text');

    $media = $this->reloadEntity($media);
    $this->assertEquals('new alt text', $media->field_media_image[0]->alt);
  }

  /**
   * Tests with a media reference field on a page URL we can't figure out.
   */
  public function testBadPageUrlWithMediaField() {
    // Create the node type, media type, and reference field.
    $node_type_storage = $this->entityTypeManager->getStorage('node_type');
    $type = $node_type_storage->create([
      'type' => 'page',
      'name' => 'Page',
    ]);
    $type->save();

    $this->createMediaType('image', ['id' => 'test_image']);

    $this->createEntityReferenceField(
      'node',
      'page',
      'field_image',
      'Image',
      'media',
    );

    // Create the file, media, and node entities.
    $this->fileSystem->copy($this->root . '/core/misc/druplicon.png', 'public://druplicon.png');
    $image_file = $this->entityTypeManager->getStorage('file')->create([
      'uri' => 'public://druplicon.png',
    ]);
    $image_file->save();

    $media = $this->entityTypeManager->getStorage('media')->create([
      'bundle' => 'test_image',
      'field_media_image' => $image_file,
    ]);
    $media->save();

    $node = $this->createNode([
      'type' => 'page',
      'field_image' => $media,
      'path' => ['alias' => '/foo'],
    ]);
    $node->save();

    $image_url = $image_file->createFileUrl();
    // Add a token, as image URLs copied by users will have these.
    $image_url .= '?itok=5IYrswxV';

    // Import with lenient update all option: the alt text will be updated even
    // though the URL is no good.
    $this->altTextImporter->importAltText($image_url, 'http://example.com/bad-page-does-not-exist', 'new alt text', no_page_url_match_update_all: TRUE);

    $media = $this->reloadEntity($media);
    $this->assertEquals('new alt text', $media->field_media_image[0]->alt);

    // Import without lenient update all option: throws an exception.
    $this->expectException('InvalidArgumentException');
    $this->expectExceptionMessageMatches('@No host entity found for the path@');
    $this->altTextImporter->importAltText($image_url, 'http://example.com/bad-page-does-not-exist', 'new alt text', no_page_url_match_update_all: FALSE);
  }

  /**
   * Tests the alt text importer service with an image field.
   */
  public function testImageField() {
    // Create the node type, and reference field.
    $node_type_storage = $this->entityTypeManager->getStorage('node_type');
    $type = $node_type_storage->create([
      'type' => 'page',
      'name' => 'Page',
    ]);
    $type->save();

    $this->createImageField(
      'field_image',
      'page',
    );

    // Create the file, and node entities.
    $this->fileSystem->copy($this->root . '/core/misc/druplicon.png', 'public://druplicon.png');
    $image_file = $this->entityTypeManager->getStorage('file')->create([
      'uri' => 'public://druplicon.png',
    ]);
    $image_file->save();

    $node = $this->createNode([
      'type' => 'page',
      'field_image' => $image_file,
      'path' => ['alias' => '/foo'],
    ]);
    $node->save();

    $stored_alias = $this->aliasRepository->lookupBySystemPath('/' . $node->toUrl()->getInternalPath(), $node->language()->getId());
    $this->assertEquals('/foo', $stored_alias['alias']);

    // Get the image URL. Note that this looks totally broken because of a core
    // bug, but will work in AltTextImporter because of how other paths are
    // returned by the test system.
    // See https://www.drupal.org/project/drupal/issues/3401247.
    // This gets us a broken URL of the form
    // /vfs://root/sites/simpletest/38522002/files/druplicon.png.
    $image_url = $image_file->createFileUrl();
    // Add a token, as image URLs copied by users will have these.
    $image_url .= '?itok=5IYrswxV';

    $this->altTextImporter->importAltText($image_url, 'http://example.com/foo', 'new alt text');

    $node = $this->reloadEntity($node);
    $this->assertEquals('new alt text', $node->field_image[0]->alt);

    // Import with media only option: throws an exception because the image
    // can't be found on a media host.
    $this->expectException('InvalidArgumentException');
    $this->expectExceptionMessageMatches('@No host entity found for the path@');
    $this->altTextImporter->importAltText($image_url, 'http://example.com/foo', 'new alt text', update_media_only: TRUE);
  }

  /**
   * Tests with an image field on a block_content entity.
   */
  public function testBlockContentField() {
    // Create a block content type with an image field.
    $block_content_type_storage = $this->entityTypeManager->getStorage('block_content_type');
    $block_content_type_storage->create([
      'id'          => 'has_image',
      'label'       => 'Block type with image',
      'description' => 'Provides a basic block type',
    ])->save();

    $this->createGenericImageField(
      'field_image',
      'block_content',
      'has_image',
    );

    // Create the file.
    $this->fileSystem->copy($this->root . '/core/misc/druplicon.png', 'public://druplicon.png');
    $image_file = $this->entityTypeManager->getStorage('file')->create([
      'uri' => 'public://druplicon.png',
    ]);
    $image_file->save();

    // Create a content block which has an image, and place the derived block.
    $block_content = $this->entityTypeManager->getStorage('block_content')->create([
      'type' => 'has_image',
      'field_image' => $image_file,
    ]);
    $block_content->save();
    $block_plugin_id = 'block_content' . PluginBase::DERIVATIVE_SEPARATOR . $block_content->uuid();

    $this->placeBlock($block_plugin_id);

    // Create the node type.
    $node_type_storage = $this->entityTypeManager->getStorage('node_type');
    $type = $node_type_storage->create([
      'type' => 'page',
      'name' => 'Page',
    ]);
    $type->save();

    // Create a node where the block is visible.
    $node = $this->createNode([
      'type' => 'page',
      'path' => ['alias' => '/foo'],
    ]);
    $node->save();

    $image_url = $image_file->createFileUrl();
    // Add a token, as image URLs copied by users will have these.
    $image_url .= '?itok=5IYrswxV';

    // Pass the lenient option to update block content entities.
    $this->altTextImporter->importAltText($image_url, 'http://example.com/foo', 'new alt text', no_page_url_match_update_all: TRUE);

    $block_content = $this->reloadEntity($block_content);
    $this->assertEquals('new alt text', $block_content->field_image[0]->alt);
  }

  /**
   * Generic version of createImageField().
   *
   * @todo Remove this when https://www.drupal.org/project/drupal/issues/3057070
   * is fixed.
   */
  protected function createGenericImageField($name, $entity_type_id, $bundle, $storage_settings = [], $field_settings = [], $widget_settings = [], $formatter_settings = [], $description = '') {
    $this->entityTypeManager->getStorage('field_storage_config')->create([
      'field_name' => $name,
      'entity_type' => $entity_type_id,
      'type' => 'image',
      'settings' => $storage_settings,
      'cardinality' => !empty($storage_settings['cardinality']) ? $storage_settings['cardinality'] : 1,
    ])->save();

    $field_config = $this->entityTypeManager->getStorage('field_config')->create([
      'field_name' => $name,
      'label' => $name,
      'entity_type' => $entity_type_id,
      'bundle' => $bundle,
      'required' => !empty($field_settings['required']),
      'settings' => $field_settings,
      'description' => $description,
    ]);
    $field_config->save();

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository->getFormDisplay($entity_type_id, $bundle)
      ->setComponent($name, [
        'type' => 'image_image',
        'settings' => $widget_settings,
      ])
      ->save();

    $display_repository->getViewDisplay($entity_type_id, $bundle)
      ->setComponent($name, [
        'type' => 'image',
        'settings' => $formatter_settings,
      ])
      ->save();

    return $field_config;
  }

  /**
   * @todo Remove when https://www.drupal.org/project/drupal/issues/3224276
   * finally gets in.
   */
  protected function reloadEntity(EntityInterface $entity) {
    $controller = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    $controller->resetCache([$entity->id()]);
    return $controller->load($entity->id());
  }

}
