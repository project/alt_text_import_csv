# Alt Text Import CSV

The Alt Text Import CSV module allows mass updating of alt texts on images, by
uploading a prepared CSV file.

## Requirements

This module requires the following modules:

- [Multi-value form element](https://www.drupal.org/project/multivalue_form_element)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

If the Entity Usage dependency module was not already enabled, go to
Administration » Configuration » Entity Usage » Batch Update and recreate the
entity usage statistics.

## Usage

The CSV file must be in the following format:

```
Page URL, Image URL, Alt text
```

To update alt texts:

1. Go to Administration » Configuration » Media
2. Upload the CSV file.

## Configuration

The module can optionally send email reports detailing any CSV rows which have
failed to import. To configure sending reports:

1. Go to Administration » Configuration » Media » Alt text CSV import
2. Enter one or more email addresses
