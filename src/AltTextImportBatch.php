<?php

namespace Drupal\alt_text_import_csv;

use Drupal\Core\Url;
use Drupal\file\FileInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Batch callbacks for AltTextImportForm.
 *
 * @todo Convert this to a service when
 * https://www.drupal.org/project/drupal/issues/3401873 is fixed.
 */
class AltTextImportBatch {

  /**
   * Implements callback_batch_operation().
   *
   * Runs for each line of the CSV.
   *
   * @param int $fid
   *   The file entity ID.
   * @param string $delimiter
   *   The delimiter character of the CSV file.
   */
  public static function processRow(int $fid, $delimiter, &$context) {
    // Setup tasks for the first run.
    if (!isset($context['sandbox']['csv_line'])) {
      // Set up the sandbox.
      $context['sandbox']['handle'] = NULL;
      $context['sandbox']['pointer_position'] = 0;
      $context['sandbox']['csv_line'] = 0;
      $context['results']['success'] = [];
      $context['results']['error'] = [];

      // Set up the private tempstore for the results.
      /** @var \Drupal\Core\TempStore\PrivateTempStoreFactory $tempstore */
      $tempstore = \Drupal::service('tempstore.private');
      $store = $tempstore->get('alt_text_import_csv');
      $store->set("results_$fid", []);
      $context['results']['fid'] = $fid;
    }

    // Get the handle if we don't currently have it.
    if (!is_resource($context['sandbox']['handle'])) {
      $file_entity = static::getFile($fid);
      $context['sandbox']['handle'] = fopen($file_entity->getFileUri(), 'r');

      // Move the pointer to where it was for the last operation.
      fseek($context['sandbox']['handle'], $context['sandbox']['pointer_position']);
    }

    // If we've reached the end of the file, we're done.
    if (feof($context['sandbox']['handle'])) {
      $context['finished'] = 1;

      return;
    }

    // We're not finished until we reach the end of the CSV file.
    $context['finished'] = 0;

    // Get the data, and update our stored pointer.
    $csv_row = fgetcsv(
      stream: $context['sandbox']['handle'],
      separator: $delimiter,
    );
    $current_csv_line_number = $context['sandbox']['csv_line'];
    $context['sandbox']['csv_line']++;
    $context['sandbox']['pointer_position'] = ftell($context['sandbox']['handle']);

    [$page_url, $image_url, $alt_text] = $csv_row;

    // Allow the first row to be a header.
    if ($current_csv_line_number == 0 && !filter_var($page_url, FILTER_VALIDATE_URL) && !filter_var($image_url, FILTER_VALIDATE_URL)) {
      $context['results']['header_skipped'] = TRUE;

      return;
    }

    // Silently skip a row with an empty image URL or alt text.
    if (empty($image_url) || empty($alt_text)) {
      return;
    }

    if (!filter_var($page_url, FILTER_VALIDATE_URL)) {
      static::addError($fid, $current_csv_line_number, $csv_row, t("Invalid page URL '@url'.", [
        '@url' => $page_url,
      ]));
      return;
    }
    if (!filter_var($image_url, FILTER_VALIDATE_URL)) {
      static::addError($fid, $current_csv_line_number, $csv_row, t("Invalid image URL '@url'.", [
        '@url' => $image_url,
      ]));
      return;
    }
    if (empty($alt_text)) {
      static::addError($fid, $current_csv_line_number, $csv_row, t("Empty alt text."));
      return;
    }

    // Get configuration.
    $config = \Drupal::service('config.factory')->get('alt_text_import_csv.settings');

    try {
      /** @var \Drupal\alt_text_import_csv\AltTextImporter $alt_text_importer */
      $alt_text_importer = \Drupal::service('alt_text_import_csv.alt_text_importer');
      $alt_text_importer->importAltText(
        image_url: $image_url,
        page_url: $page_url,
        alt_text: $alt_text,
        no_page_url_match_update_all: $config->get('no_page_url_match_update_all'),
        update_media_only: $config->get('media_only'),
      );

      $context['results']['success'][] = $current_csv_line_number;
    }
    catch (\Exception $e) {
      static::addError($fid, $current_csv_line_number, $csv_row, $e->getMessage());
    }
  }

  /**
   * Adds an error to the stored results.
   *
   * @param int $fid
   *   The file ID for the CSV file being imported.
   * @param int $line_number
   *   The CSV file line number where the error was found.
   * @param array $csv_row
   *   The CSV row data.
   * @param string $message
   *   The error message.
   */
  protected static function addError(int $fid, int $line_number, array $csv_row, string $message): void {
    /** @var \Drupal\Core\TempStore\PrivateTempStoreFactory $tempstore */
    $tempstore = \Drupal::service('tempstore.private');
    $store = $tempstore->get('alt_text_import_csv');
    $results_data = $store->get("results_$fid", []);
    $results_data[] = [
      'line_number' => $line_number,
      'row' => $csv_row,
      'message' => $message,
    ];
    $store->set("results_$fid", $results_data);
  }

  /**
   * Implements callback_batch_finished().
   */
  public static function finish($success, $results, $operations, $elapsed) {
    $messenger = \Drupal::messenger();

    if ($success) {
      if (count($results['success'])) {
        $messenger->addMessage(t("Imported alt texts from @count rows.", [
          '@count' => count($results['success']),
        ]));
      }

      if (!empty($results['header_skipped'])) {
        $messenger->addMessage(t("A header row was detected in the CSV file and was skipped."));
      }

      // Send report mail if there were any errors.
      /** @var \Drupal\Core\TempStore\PrivateTempStoreFactory $tempstore */
      $tempstore = \Drupal::service('tempstore.private');
      $store = $tempstore->get('alt_text_import_csv');
      $results_data = $store->get("results_" . $results['fid'], []);
      if (count($results_data)) {
        static::sendReportMail($results['fid']);
      }

      // Redirect to the results page.
      return new RedirectResponse(Url::fromRoute(
        'alt_text_import_csv.import.results', [
          'file' => $results['fid'],
        ]
      )->toString());
    }
    else {
      $error_operation = reset($operations);
      $messenger->addMessage(t('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]), 'error');
    }
  }

  /**
   * Sends report emails if the module has been configured to do so.
   *
   * @param int $fid
   *   The file ID of the uploaded CSV file.
   *
   * @see \Drupal\alt_text_import_csv\Form\AdminSettingsForm
   */
  protected static function sendReportMail(int $fid) {
    $mails = \Drupal::service('config.factory')->get('alt_text_import_csv.settings')->get('mails');
    if (empty($mails)) {
      return;
    }
    $mails = implode(', ', $mails);

    $langcode = \Drupal::service('language.default')->get()->getId();

    $params = [
      'uid' => \Drupal::service('current_user')->id(),
      'fid' => $fid,
    ];

    \Drupal::service('plugin.manager.mail')->mail('alt_text_import_csv', 'report', $mails, $langcode, $params);
  }

  /**
   * Loads a file entity.
   *
   * @param int $fid
   *   The file entity ID.
   *
   * @return \Drupal\file\FileInterface
   *   The file entity.
   */
  protected static function getFile(int $fid): FileInterface {
    return \Drupal::service('entity_type.manager')->getStorage('file')->load($fid);
  }

}
