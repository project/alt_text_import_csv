<?php

namespace Drupal\alt_text_import_csv\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\file\FileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a results page for the CSV import batch.
 */
class ImportResultsController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The private temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $privateTempStoreFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
    );
  }

  /**
   * Creates a ImportResultsController instance.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $private_temp_store_factory
   *   The private temp store factory.
   */
  public function __construct(
    PrivateTempStoreFactory $private_temp_store_factory
  ) {
    $this->privateTempStoreFactory = $private_temp_store_factory;
  }

  /**
   * Title callback for the alt_text_import_csv.import.results route.
   */
  public function title(FileInterface $file) {
    return $this->t('Import results for %file', [
      '%file' => $file->label(),
    ]);
  }

  /**
   * Callback for the alt_text_import_csv.import.results route.
   */
  public function content(FileInterface $file) {
    $build = [];

    $store = $this->privateTempStoreFactory->get('alt_text_import_csv');
    $results_error_data = $store->get("results_" . $file->id());

    if (count($results_error_data)) {
      $build['intro'] = [
        '#markup' => $this->formatPlural(
          count($results_error_data),
          "The following CSV row from %file did not import:",
          "The following @count CSV rows from %file  did not import:",
          [
            '%file' => $file->label(),
          ],
        ),
      ];

      $rows = [];
      foreach ($results_error_data as $error_item) {
        [$page_url, $image_url, $alt_text] = $error_item['row'];

        $rows[] = [
          // Line numbers are zero-indexed, so increase this by 1 as most UIs
          // such as spreadsheet apps and text editors start at 1.
          $error_item['line_number'] + 1,
          $page_url,
          $image_url,
          $error_item['message'],
        ];
      }

      $build['results'] = [
        '#type' => 'table',
        '#header' => [
          $this->t('Row number'),
          $this->t('Page URL'),
          $this->t('Image URL'),
          $this->t('Error message'),
        ],
        '#rows' => $rows,
      ];
    }

    $build['import_another'] = [
      '#type' => 'link',
      '#title' => $this->t("Import another"),
      '#url' => Url::fromRoute('alt_text_import_csv.import'),
    ];

    return $build;
  }

}
