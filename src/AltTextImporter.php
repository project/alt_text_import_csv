<?php

namespace Drupal\alt_text_import_csv;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\file\FileInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\path_alias\AliasManagerInterface;

/**
 * Imports alt texts given page and image URLs.
 */
class AltTextImporter {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * The alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * Creates a AltTextImporter instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository service.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The alias manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    FileRepositoryInterface $file_repository,
    AliasManagerInterface $alias_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fileRepository = $file_repository;
    $this->aliasManager = $alias_manager;
  }

  /**
   * Imports alt text.
   *
   * This will attempt to find host entities which are visible on the given page
   * URL, however it has the following limitations for determining visibility:
   *  - An entity is only detected as being visible in another if Entity Usage
   *    reports it as such. That will not work for entities such as
   *    block_content placed as sitewide blocks, or entities shown in embedded
   *    views.
   *  - The given page URL is matched with the canonical URL of an entity. If
   *    an entity is shown at a different URL, it will not be detected.
   *
   * The behaviour in the case that nothing matches the page URL depends on the
   * $no_page_url_match_update_all parameter.
   *
   * @param string $image_url
   *   The URL of the image to update.
   * @param string $page_url
   *   The URL of the page on which the image is seen.
   * @param string $alt_text
   *   The new alt text to set.
   * @param bool $no_page_url_match_update_all
   *   (optional) Whether to update the image on all host entities if no host
   *   entities can be found which match the given page URL. This is useful for
   *   things such as block_content entities, which the Entity Usage module
   *   won't find as being used by a particular node. If FALSE, an exception is
   *   thrown if no host entities can be found. Defaults to FALSE.
   * @param bool $update_media_only
   *   (optional) Whether to only update media host entities. Defaults to FALSE.
   *
   * @throws \InvalidArgumentException
   *   Throws an exception if nothing could be updated:
   *   - If $no_page_url_match_update_all is FALSE, and no host entity could be
   *     found for the given page URL.
   *   - If no host entities could be found at all.
   */
  public function importAltText(string $image_url, string $page_url, string $alt_text, bool $no_page_url_match_update_all = FALSE, bool $update_media_only = FALSE) {
    $file = $this->getImageFromUrl($image_url);

    $path = $this->preparePath($page_url);

    $host_entity_items = $this->findHostEntities(
      $file,
      $path,
      no_page_url_match_return_all: $no_page_url_match_update_all,
      media_hosts_only: $update_media_only,
    );

    if (empty($host_entity_items)) {
      throw new \InvalidArgumentException("No host entity found for the path $page_url and file entity {$file->id()}.");
    }

    foreach ($host_entity_items as $host_entity_item) {
      $host_entity = $host_entity_item->hostEntity;
      foreach ($host_entity_item->fieldNames as $field_name) {
        $referenced_files = $host_entity->get($field_name)->referencedEntities();

        foreach ($referenced_files as $delta => $referenced_file) {
          if ($referenced_file->id() == $file->id()) {
            // Update the alt text on the host entity.
            $host_entity->get($field_name)->get($delta)->get('alt')->setValue($alt_text);

            $alt_texts_updated[] = TRUE;
          }
        }
      }

      $host_entity->save();
    }

    if (empty($alt_texts_updated)) {
      throw new \InvalidArgumentException("The file was not found in the entity at the given path.");
    }
  }

  /**
   * Gets a path from the given page URL.
   *
   * This does the following processing:
   *  - The domain and protocol are removed.
   *  - The base path is removed: this takes care of the case when Drupal is
   *    running in a subfolder.
   *  - An empty path is resolved to the front page path configuration setting.
   *
   * @param string $page_url
   *   The given page URL.
   *
   * @return string
   *   A local path.
   */
  protected function preparePath(string $page_url): string {
    // Get the path from the URL.
    $path = parse_url($page_url, PHP_URL_PATH);
    // Remove the base path, if Drupal is running in a subfolder. We subtract 1
    // from the string length because the base path includes the terminal '/'
    // but we need it at the start of the path.
    $path = substr($path, strlen(base_path()) - 1);

    if ($path == '/') {
      $config = \Drupal::config('system.site');
      $front_page = $config->get('page.front');

      $path = $front_page;
    }

    return $path;
  }

  /**
   * Finds the host entity for the image file, based on the page URL.
   *
   * The host entity is not necessarily the entity that is being displayed at
   * the page URL. This is because the image could be referenced by an embedded
   * entity such as a paragraph, or a referenced node.
   *
   * For example, we could something like one of the following:
   *   node -> paragraph -> media -> image
   *   node -> paragraph -> paragraph -> media -> image
   *   node -> node -> paragraph -> image
   *
   * The host entity is the entity which points directly to the image entity,
   * although we have to ascent to the top of the references chain to check the
   * page URL.
   *
   * @param \Drupal\file\FileInterface $file_entity
   *   The file entity that was found for the image URL.
   * @param string $page_url
   *   The page URL.
   * @param bool $no_page_url_match_return_all
   *   Whether to return all host entities if none are found which match the
   *   given page URL.
   * @param bool $media_hosts_only
   *   Whether to only return media entities as hosts.
   *
   * @return \Drupal\alt_text_import_csv\HostEntityItem[]
   *   An array of host entity value objects. This will contain either:
   *    - The host entities that match the page URL, if any were found.
   *    - All host entities if none were found that match the page URL.
   */
  protected function findHostEntities(FileInterface $file_entity, string $page_url, bool $no_page_url_match_return_all, bool $media_hosts_only): array {
    $sources = \Drupal::service('entity_usage.usage')->listSources($file_entity);

    if ($media_hosts_only) {
      $sources = array_intersect_key($sources, ['media' => TRUE]);
    }

    $all_host_entity_items = [];
    $matching_host_entity_items = [];

    foreach ($sources as $entity_type_id => $entity_type_sources) {
      foreach ($entity_type_sources as $entity_id => $entity_sources) {
        /** @var \Drupal\Core\Entity\ContentEntityInterface $source_entity */
        $host_entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);

        $field_names = array_column($entity_sources, 'field_name');
        $host_entity_item = new HostEntityItem($host_entity, $field_names);

        if ($this->entityOrParentUsesPageUrl($host_entity, $page_url)) {
          $matching_host_entity_items[] = $host_entity_item;
        }

        $all_host_entity_items[] = $host_entity_item;
      }
    }

    if ($no_page_url_match_return_all) {
      return $matching_host_entity_items ?: $all_host_entity_items;
    }
    else {
      return $matching_host_entity_items;
    }
  }

  /**
   * Determines whether an entity or a reference source is at the page URL.
   *
   * This ascends chains of referencing entities, looking for an entity whose
   * URL is the given page URL.
   *
   * For example, we could have one of the following:
   *   node -> paragraph -> media
   *   node -> node
   *
   * If multiple entities point to an entities, all of the reference chains are
   * ascended.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to check. This is a source of the file entity whose ALT text
   *   is being updated.
   * @param string $page_url
   *   The page URL.
   *
   * @return bool
   *   TRUE if the page URL is the URL for one of the entities in a reference
   *   chain from the given entity (including the given entity itself). FALSE if
   *   nothing was found.
   */
  public function entityOrParentUsesPageUrl(ContentEntityInterface $entity, string $page_url): bool {
    $template = $entity->getEntityType()->getLinkTemplate('canonical');

    // We skip a canonical template that's the edit form, such as for media
    // entities.
    if ($template && !str_ends_with($template, 'edit')) {
      $uri = $entity->toUrl()->toString();
      $alias = $this->aliasManager->getAliasByPath($uri);

      if ($alias == $page_url) {
        return TRUE;
      }
    }

    // If we're still here, either the entity is nested (such as media,
    // paragraph, etc) or we didn't find an alias and it could be a node
    // that is referenced and displayed in its referencer.
    // Look at the referencing entities recursively.
    $sources = \Drupal::service('entity_usage.usage')->listSources($entity);
    foreach ($sources as $entity_type_id => $entity_type_sources) {
      foreach ($entity_type_sources as $entity_id => $entity_sources) {
        /** @var \Drupal\Core\Entity\ContentEntityInterface $source_entity */
        $host_entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
        $result = $this->entityOrParentUsesPageUrl($host_entity, $page_url);
        if ($result) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Gets a file entity from the image URL.
   *
   * @param string $image_url
   *   The image URL.
   *
   * @return \Drupal\file\FileInterface
   *   The file entity.
   *
   * @throws \InvalidArgumentException
   *   Throws an InvalidArgumentException if no file entity can be found.
   */
  protected function getImageFromUrl(string $image_url): FileInterface {
    // Trim off the domain.
    $path = parse_url($image_url, PHP_URL_PATH);

    // Trim off the base path and public file path.
    $public_file_path = PublicStream::basePath();
    $image_file_path = str_replace(base_path() . $public_file_path . '/', '', $path);

    // Account for images that are an image style derivative. In this case,
    // the URL has a prefix of the form: 'styles/STYLE_ID/SOURCE_SCHEME/'.
    // @todo Consider the corner case where the actual folder to save the
    // image has this in the path?
    $image_file_path = preg_replace('@styles/\w+/\w+/@', '', $image_file_path);
    $image_uri = 'public://' . $image_file_path;

    $file = $this->fileRepository->loadByUri($image_uri);

    if (!$file) {
      throw new \InvalidArgumentException("No image found for the path $image_url.");
    }

    return $file;
  }

}
