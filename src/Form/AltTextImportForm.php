<?php

namespace Drupal\alt_text_import_csv\Form;

use Drupal\alt_text_import_csv\AltTextImportBatch;
use Drupal\Component\Utility\Environment;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to import a CSV file to update alt texts.
 */
class AltTextImportForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Creates a AltTextImportForm instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'alt_text_import_csv_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['csv_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Upload CSV file'),
      '#required' => TRUE,
      '#description' => $this->t('A CSV file whose columns are: page URL, image URL, alt text. The CSV file may contain a header row.'),
      '#upload_location' => 'public://metatag_import_export_csv/',
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
        'file_validate_size' => [Environment::getUploadMaxSize()],
      ],
    ];

    $form['delimiter'] = [
      '#title' => $this->t('Delimiter'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#description' => $this->t('The delimiter character used in the CSV file.'),
      '#default_value' => ',',
      '#maxlength' => 2,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#required' => TRUE,
      '#value' => $this->t('Import'),
      '#button_type' => 'danger',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // We have to validate the file element like chumps because core doesn't do
    // it. See https://www.drupal.org/project/drupal/issues/2938441.
    if (!isset($form_state->getValue('csv_file')[0])) {
      $form_state->setErrorByName('csv_file', $this->t("The CSV file is required."));
      return;
    }

    $fid = $form_state->getValue('csv_file')[0];
    if (!is_numeric($fid) || !($fid > 0)) {
      $form_state->setErrorByName('csv_file', $this->t("The CSV file can't be found."));
      return;
    }

    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    $delimiter = $form_state->getValue('delimiter');

    // Check if it is possible to open the file.
    $handle = fopen($file->getFileUri(), 'r');

    if (!$handle) {
      $form_state->setErrorByName('csv_file', $this->t("The CSV file can't be opened."));
    }
    else {
      $headers = trim(fgets($handle));
      // Close the CSV file, as the batch setup needs to open it from the top.
      fclose($handle);

      // Check if file used correct delimiter.
      if (!str_contains($headers, $delimiter)) {
        $form_state->setErrorByName('csv_file', $this->t("The given delimiter '@delimiter' was not found in the CSV file.", [
          '@delimiter' => $delimiter,
        ]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uploads = $form_state->getValue('csv_file');
    $delimiter = $form_state->getValue('delimiter');
    $fid = $uploads[0];

    $batch_builder = (new BatchBuilder())
      ->setTitle($this->t('Importing CSV file'))
      ->setFinishCallback([AltTextImportBatch::class, 'finish'])
      ->setInitMessage($this->t('Starting import.'))
      ->setProgressMessage($this->t('Completed @current row of @total.'))
      ->setErrorMessage($this->t('CSV import has encountered an error.'));

    $batch_builder->addOperation(
      [AltTextImportBatch::class, 'processRow'],
      [
        $fid,
        $delimiter,
      ]
    );

    batch_set($batch_builder->toArray());
  }

}
