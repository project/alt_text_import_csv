<?php

namespace Drupal\alt_text_import_csv\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\multivalue_form_element\Element\MultiValue;

/**
 * Settings form for alt text import CSV module.
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['alt_text_import_csv.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'alt_text_import_csv_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('alt_text_import_csv.settings');

    $form['mails'] = [
      '#type' => 'multivalue',
      '#title' => $this->t('Notification recipients'),
      '#cardinality' => MultiValue::CARDINALITY_UNLIMITED,
      'mail' => [
        '#type' => 'email',
        '#title' => $this->t("Email"),
      ],
      '#description' => $this->t("The email addresses to send CSV upload reports to. To disable sending of reports, remove all email addresses."),
      '#default_value' => $config->get('mails') ?? [],
    ];

    $form['import'] = [
      '#type' => 'details',
      '#title' => $this->t('Image usage options'),
      '#open' => TRUE,
    ];

    $form['import']['no_page_url_match_update_all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update all host entities if page URL cannot be matched'),
      '#description' => $this->t("If enabled, an image for which a host entity can't be found on the given page URL will have its ALT text updated on all host entities. For example, this will allow images in Block content entities to be updated."),
      '#default_value' => $config->get('no_page_url_match_update_all') ?? FALSE,
    ];

    $form['import']['media_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update only media entity hosts'),
      '#default_value' => $config->get('media_only') ?? FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('alt_text_import_csv.settings');

    $mails = array_column($form_state->getValue('mails'), 'mail');

    $config->set('mails', $mails);

    $config->set('no_page_url_match_update_all', $form_state->getValue('no_page_url_match_update_all'));
    $config->set('media_only', $form_state->getValue('media_only'));

    $config->save();
  }

}
