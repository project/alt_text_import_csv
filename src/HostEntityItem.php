<?php

namespace Drupal\alt_text_import_csv;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Value object to represent a host entity and the reference fields.
 */
class HostEntityItem {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $hostEntity
   *   The host entity.
   * @param array $fieldNames
   *   An array of field names of the fields which point at the target entity.
   */
  public function __construct(
    public readonly ContentEntityInterface $hostEntity,
    public readonly array $fieldNames,
  ) {}

}
